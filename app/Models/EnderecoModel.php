<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EnderecoModel extends Model
{
    protected $table = "tb_endereco";
    protected $primaryKey = "nu_seq_endereco";
    protected $fillable = ['ds_rua','ds_bairro','co_cep','co_numero','ds_complemento','ds_cidade','ds_uf'];
}
