<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CursoModel extends Model
{
    protected $table = "tb_curso";
    protected $primaryKey = "nu_seq_curso";
    protected $fillable = ['ds_nome','ds_tipo'];
}
