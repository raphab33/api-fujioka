<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlunoModel extends Model
{
    protected $table = "tb_aluno";
    protected $primaryKey = "nu_seq_aluno";
    protected $fillable = ['dt_vinculado'];


    public function user(){
        return $this->belongsTo('App\Models\User', 'aluno_id', 'id');
    }


}
