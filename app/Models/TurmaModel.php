<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TurmaModel extends Model
{
    protected $table = "tb_turma";
    protected $primaryKey = "nu_seq_turma";
    protected $fillable = ['ds_demostre','dt-ano'];
}
