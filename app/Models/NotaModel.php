<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotaModel extends Model
{
    protected $table = "tb_nota";
    protected $primaryKey = "nu_seq_nota";
    protected $fillable = ['co_nota','ds_unidade','co_peso'];
}
