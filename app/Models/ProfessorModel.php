<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfessorModel extends Model
{
    //
    protected $table = "tb_professor";
    protected $primaryKey = "nu_seq_professor";
    protected $fillable = ['ds_area_atuacao','ds_titulacao'];
}
