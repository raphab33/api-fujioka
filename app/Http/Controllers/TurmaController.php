<?php

namespace App\Http\Controllers;

use App\Models\TurmaModel;
use Illuminate\Http\Request;


class TurmaController extends Controller
{
    public function index (){
        return TurmaModel::all();
    }

    public function show ($id){
        return TurmaModel::find($id);
    }

    public function store(Request $request){
        return TurmaModel::create($request->all());
    }

    public function update(Request $request, $id){
        $endereco = TurmaModel::findOrFail($id);
        $endereco->update($request->all());

        return $endereco;
    }

    public function delete(Request $request, $id){
        $endereco = TurmaModel::findOrFail($id);
        $endereco->delete();

        return 204;
    }
}
