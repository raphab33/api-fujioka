<?php

namespace App\Http\Controllers;


use App\Models\ProfessorModel;
use Illuminate\Http\Request;

class ProfessorController extends Controller
{
    private $curso_controller;


    public function index (){
        return ProfessorModel::all();
    }

    public function show ($id){
        return ProfessorModel::find($id);
    }

    public function store(Request $request){

        $endereco = ProfessorModel::create($request->all());
        if ($request->ds_titulacao && $request->ds_area_atuacao){
            $professor = new Professor();
            $professor->area_atuacao = $request->ds_area_atuacao;
            $professor->titulacao = $request->ds_titulacao;
            $this->curso_controller->store($professor);

            //return ProfessorModel::create($request->all());
        }
    }

    public function update(Request $request, $id){
        $endereco = ProfessorModel::findOrFail($id);
        $endereco->update($request->all());

        return $endereco;
    }

    public function delete(Request $request, $id){
        $endereco = ProfessorModel::findOrFail($id);
        $endereco->delete();

        return '204';
    }
}
