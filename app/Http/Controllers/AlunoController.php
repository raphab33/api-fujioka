<?php

namespace App\Http\Controllers;

use App\Models\AlunoModel;
use Illuminate\Http\Request;


class AlunoController extends Controller
{

    public function index (){
        return AlunoModel::all();
    }

    public function show ($id){
        return AlunoModel::find($id);
    }

    public function store(Request $request){

        return AlunoModel::create($request->all());
    }

    public function update(Request $request, $id){
        $endereco = AlunoModel::findOrFail($id);
        $endereco->update($request->all());

        return $endereco;
    }

    public function delete(Request $request, $id){
        $endereco = AlunoModel::findOrFail($id);
        $endereco->delete();

        return 204;
    }
}
