<?php

namespace App\Http\Controllers;

use App\Models\NotaModel;
use Illuminate\Http\Request;


class NotaController extends Controller
{
    public function index (){
        return NotaModel::all();
    }

    public function show ($id){
        return NotaModel::find($id);
    }

    public function store(Request $request){
        return NotaModel::create($request->all());
    }

    public function update(Request $request, $id){
        $nota = NotaModel::findOrFail($id);
        $nota->update($request->all());

        return $nota;
    }

    public function delete(Request $request, $id){
        $nota = NotaModel::findOrFail($id);
        $nota->delete();

        return 204;
    }
}
