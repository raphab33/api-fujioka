<?php

namespace App\Http\Controllers;

use App\Models\EnderecoModel;
use Illuminate\Http\Request;


class EnderecoController extends Controller
{
    public function index (){
        return EnderecoModel::all();
    }

    public function show ($id){
        return EnderecoModel::find($id);
    }

    public function store(Request $request){
        return EnderecoModel::create($request->all());
    }

    public function update(Request $request, $id){
        $endereco = EnderecoModel::findOrFail($id);
        $endereco->update($request->all());
        return $endereco;
    }

    public function delete(Request $request, $id){
        $endereco = EnderecoModel::findOrFail($id);
        $endereco->delete();

        return 204;
    }
}
