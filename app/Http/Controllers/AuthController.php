<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:8|confirmed'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error_code' => 10,
                'errors' => [
                    $validator->errors()
                ],
            ], 400, [], JSON_UNESCAPED_UNICODE);
        }

        User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password'))
        ]);

        return $this->login($request);
    }

    public function login(Request $request)
    {
        try {
            $credenciais = $request->only(['email', 'password']);
            if (!$token = auth()->attempt($credenciais, true)) {
                return response()->json([
                    'error_code' => 1,
                    'errors' => [
                        'error' => 'no_authenticate',
                        'message' => 'Não foi possível se autenticar, credenciais inválidas.'
                    ]
                ], 500);
            }
            return $this->respondWithToken($token);
        } catch (\Throwable $e) {
            return response()->json(
                [
                    'error_code' => $e->getCode(),
                    'errors' => [
                        'error' => 'could_not_create_token',
                        'message' => 'Não foi possível criar o token'
                    ],
                    'file' => $e->getFile(),
                    'method' => $request->getMethod(),
                    'uri' => $request->getUri(),
                    'line' => $e->getLine()
                ], 500);
        }
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function me()
    {
        return response()->json([
            'status' => 'ok',
            'user' => auth()->user()
        ]);
    }

    public function logout()
    {
        auth()->logout();
        return response()->json([
                'status' => 'ok',
                'message' => 'Logout feito com sucesso'
            ]
        );
    }

    public function index(Request $request)
    {
        return User::all();
    }

    public function show(Request $request, $user_id)
    {
        return User::find($user_id)->first();
    }

    public function update(Request $request, $user_id)
    {
        $dados = $request->all();
        $user = \App\Models\User::findOrFail($user_id);
        $user->update($dados);
        return 'ok';
    }
}
