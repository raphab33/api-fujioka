<?php

namespace App\Http\Controllers;

use App\Models\CursoModel;
use Illuminate\Http\Request;


class CursoController extends Controller
{
    public function index (){
        return CursoModel::all();
    }

    public function show ($id){
        return CursoModel::find($id);
    }

    public function store(Request $request){

        try {
            $curso_request = $request->all();
            return CursoModel::create($curso_request);
        } catch (\Exception $e){
            return "Error: " . $e->getMessage();
        }
        //return CursoModel::create($request->all());
    }
    /*public function storeCurso(Curso $curso){
        var_dump($curso);
        return CursoModel::create(Curso->all());
    }*/

    public function update(Request $request, $id){
        $curso = CursoModel::findOrFail($id)->update($request->all());;
        return $curso;
    }

    public function delete(Request $request, $id){
        CursoModel::findOrFail($id)->delete();
        return 204;
    }
}
