<?php

namespace App\Policies;

use App\App\Models\Servidor\Servidor;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ServidorPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any servidors.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //

    }

    /**
     * Determine whether the user can view the servidor.
     *
     * @param \App\Models\User $user
     * @param \App\App\Models\Servidor\Servidor $servidor
     * @return mixed
     */
    public function view(User $user, Servidor $servidor)
    {
        //
        return $user->servidor_id === $servidor->id;
    }

    /**
     * Determine whether the user can create servidors.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        //

    }

    /**
     * Determine whether the user can update the servidor.
     *
     * @param \App\Models\User $user
     * @param \App\App\Models\Servidor\Servidor $servidor
     * @return mixed
     */
    public function update(User $user, Servidor $servidor)
    {
        //
        return $user->servidor_id === $servidor->id;
    }

    /**
     * Determine whether the user can delete the servidor.
     *
     * @param \App\Models\User $user
     * @param \App\App\Models\Servidor\Servidor $servidor
     * @return mixed
     */
    public function delete(User $user, Servidor $servidor)
    {
        //

    }

    /**
     * Determine whether the user can restore the servidor.
     *
     * @param \App\Models\User $user
     * @param \App\App\Models\Servidor\Servidor $servidor
     * @return mixed
     */
    public function restore(User $user, Servidor $servidor)
    {
        //

    }

    /**
     * Determine whether the user can permanently delete the servidor.
     *
     * @param \App\Models\User $user
     * @param \App\App\Models\Servidor\Servidor $servidor
     * @return mixed
     */
    public function forceDelete(User $user, Servidor $servidor)
    {
        //

    }
}
