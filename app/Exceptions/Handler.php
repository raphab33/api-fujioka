<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Exception $exception
     * @return
     */
    public function render($request, Exception $exception)
    {
        if (!$request->expectsJson()) {
            return parent::render($request, $exception);
        }

        if ($exception instanceof ModelNotFoundException)
            return response()->json([
                'error_code' => $exception->getCode(),
                'message' => "{$exception->getModel()} com Id {$exception->getIds()[0]} não encontrado"
            ], 404);


        if ($exception instanceof TokenInvalidException)
            return response()->json(
                [
                    'error_code' => $exception->getCode(),
                    'errors' => [
                        'error' => 'invalid_token',
                        'message' => 'Token inválido'
                    ],
                    'file' => $exception->getFile(),
                    'method' => $request->getMethod(),
                    'uri' => $request->getUri(),
                    'line' => $exception->getLine(),
                    'message' => $exception->getMessage()
                ], 400);

        if ($exception instanceof TokenExpiredException)
            return response()->json(
                [
                    'error_code' => $exception->getCode(),
                    'errors' => [
                        'error' => 'expired_token',
                        'message' => 'Token expirado'
                    ],
                    'file' => $exception->getFile(),
                    'method' => $request->getMethod(),
                    'uri' => $request->getUri(),
                    'line' => $exception->getLine(),
                    'message' => $exception->getMessage()
                ], 400);

        if ($exception instanceof ValidationException)
            return response()->json([
                'error_code' => 10,
                'errors' => [
                    $exception->errors()
                ],
            ], 422, [], JSON_UNESCAPED_UNICODE);

        if ($exception instanceof NotFoundHttpException)
            return response(null, 404);

        return response()->json(
            [
                'error_code' => $exception->getCode(),
                'errors' => [
                    'error' => 'intern_error',
                    'message' => 'Erro interno no servidor'
                ],
                'file' => $exception->getFile(),
                'method' => $request->getMethod(),
                'uri' => $request->getUri(),
                'line' => $exception->getLine(),
                'message' => $exception->getMessage(),
                'className' => get_class($exception)
            ], 500);
    }
}
