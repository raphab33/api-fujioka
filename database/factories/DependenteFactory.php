<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Servidor\Dependente;
use Faker\Generator as Faker;
use Illuminate\Support\Collection;

$factory->define(Dependente::class, function (Faker $faker) {
    return [
        'nome_completo' => $faker->name,
        'data_nascimento' => $faker->date,
        'rg_numero' => $faker->rg(false),
        'rg_data_expedicao' => $faker->date,
        'rg_estado' => $faker->stateAbbr,
        'rg_orgao_emissor' => Collection::make(['SDS', 'SSP', 'SDM'])->random(),
        'genero' => Collection::make(['m', 'f', 'o'])->random(),
        'parentesco' => Collection::make(['Filho', 'Primo', 'Marido', 'Esposa'])->random(),
        'cpf' => $faker->cpf(false),
    ];
});
