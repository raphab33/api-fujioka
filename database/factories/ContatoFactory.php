<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Servidor\Contato;
use App\Models\Servidor\Servidor;
use Faker\Generator as Faker;
use Illuminate\Support\Collection;

$factory->define(Contato::class, function (Faker $faker) {
    return [
        //
        'ddd' => $faker->areaCode,
        'numero' => $faker->cellPhone(false),
        'operadora' => Collection::make(['claro', 'vivo', 'oi', 'tim'])->random(),
        'servidor_id' => Servidor::all('id')->random()
    ];
});
