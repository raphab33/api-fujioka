<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Servidor\Documentacao;
use App\Models\Servidor\Servidor;
use Faker\Generator as Faker;
use Illuminate\Support\Collection;

$factory->define(Documentacao::class, function (Faker $faker) {
    return [
        'nome_mae' => $faker->name('female'),
        'nome_pai' => $faker->name('male'),
        'data_nascimento' => $faker->date,
        'cpf' => $faker->cpf(false),
        'pis_pasep' => $faker->numerify('#########'),
        'rg_numero' => $faker->rg,
        'rg_orgao_emissor' => Collection::make(['SDS', 'SSP', 'SDM'])->random(),
        'rg_estado' => $faker->stateAbbr,
        'rg_data_expedicao' => $faker->date,
        'titulo_eleitor_numero' => $faker->numerify('################'),
        'titulo_eleitor_estado' => $faker->stateAbbr,
        'titulo_eleitor_zona' => $faker->numerify('###'),
        'titulo_eleitor_sessao' => $faker->numerify('####'),
        'cnh_numero' => $faker->numerify('#############'),
        'chh_categoria' => Collection::make(['A', 'B', 'C', 'D', 'E', 'AB', 'AC', 'AD', 'AE'])->random(),
        'cnh_validade' => $faker->date,
        'cnh_estado' => $faker->stateAbbr,
        'servidor_id' => Servidor::all()->each(function ($servidor) {
            return $servidor->documentacao == null ? $servidor : null;
        })->map(function ($servidor) {
            return $servidor->id;
        })->random()
    ];
});
