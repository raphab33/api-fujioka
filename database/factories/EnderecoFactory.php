<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Servidor\Endereco;
use App\Models\Servidor\Servidor;
use Faker\Generator as Faker;

$factory->define(Endereco::class, function (Faker $faker) {
    return [
        'cidade' => $faker->city,
        'estado' => $faker->stateAbbr,
        'codigo_postal' => $faker->numerify('########'),
        'logradouro' => $faker->streetAddress,
        'complemento' => $faker->secondaryAddress,
        'numero' => $faker->numerify('##'),
        'servidor_id' => Servidor::all(['id'])->random()
    ];
});
