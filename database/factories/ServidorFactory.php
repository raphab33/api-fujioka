<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Servidor\Servidor;
use Faker\Generator as Faker;
use Illuminate\Support\Collection;

$factory->define(Servidor::class, function (Faker $faker) {
    return [
        'tipo' => Collection::make(['servidor', 'contratado', 'terceirizado'])->random(),
        'genero' => Collection::make(['m', 'f', 'o'])->random(),
        'matricula' => $faker->numberBetween(8000, 2658879),
        'nome_completo' => $faker->name,
        'naturalidade' => $faker->city,
        'nacionalidade' => Collection::make(['BRASILEIRA', 'PORTUGUESA'])->random(),
        'tipo_sangue' => Collection::make(['A+', 'A-', 'B+', 'B-', 'AB+', 'AB-', 'O+', 'O-'])->random(),
        'estado_civil' => Collection::make(['solteiro', 'casado', 'separado', 'divorciado', 'viuvo'])->random(),
        'etnia' => Collection::make(['branca', 'preta', 'parda', 'amarela', 'indigena'])->random(),
        'cor_olhos' => $faker->colorName,
        'cor_cabelo' => $faker->colorName,
        'altura_cm' => $faker->numberBetween(160, 220),
        'religiao' => Collection::make(['Católico', 'Protestante', 'Ateu', 'Não Definida'])->random()
    ];
});
