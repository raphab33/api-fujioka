<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Servidor\Funcional;
use App\Models\Servidor\Servidor;
use Faker\Generator as Faker;
use Illuminate\Support\Collection;

$factory->define(Funcional::class, function (Faker $faker) {

    return [
        'local_trabalho' => $faker->address,
        'tipo_servidor' => Collection::make(['matriculado', 'concursado', 'terceirizado', 'temporario'])->random(),
        'funcao' => $faker->jobTitle,
        'data_alteracao_funcao' => $faker->date,
        'data_admissao' => $faker->date,
        'tipo_ingresso' => 'op',
        'processo_judicial' => Collection::make([$faker->numerify('########'), null])->random(),
        'orgao_vinculado' => Collection::make(['estado', 'local'])->random(),
        'servidor_id' => Servidor::all()->each(function ($servidor) {
            return $servidor->funcional == null ? $servidor : null;
        })->map(function ($servidor) {
            return $servidor->id;
        })->random()
    ];
});
