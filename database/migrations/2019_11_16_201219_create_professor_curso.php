<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfessorCurso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profesor_curso', function (Blueprint $table) {
            $table->unsignedBigInteger('nu_seq_curso_id');
            $table->unsignedBigInteger('nu_seq_professor_id');
            $table->foreign('nu_seq_curso_id')->references('nu_seq_curso')->on('tb_curso')
                ->onDelete('cascade');
            $table->foreign('nu_seq_professor_id')->references('nu_seq_professor')->on('tb_professor')
                ->onDelete('cascade');
            $table->primary(['nu_seq_curso_id','nu_seq_professor_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profesor_curso');
    }
}
