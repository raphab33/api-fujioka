<?php

use App\Models\Servidor\Funcional;
use Illuminate\Database\Seeder;

class FuncionalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(Funcional::class, 10)->create();
    }
}
