<?php

use App\Models\Servidor\Dependente;
use App\Models\Servidor\Servidor;
use Illuminate\Database\Seeder;

class DependenteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(Dependente::class, 10)->create()->each(function ($dependente) {
            $dependente->servidores()->save(factory(Servidor::class)->make());
        });
    }
}
