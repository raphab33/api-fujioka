<?php

use App\Models\Servidor\Servidor;
use Illuminate\Database\Seeder;

class ServidorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Servidor::class, 30)->create();
    }
}
