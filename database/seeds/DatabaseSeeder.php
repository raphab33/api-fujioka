<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call([
            ServidorSeeder::class,
            ContatoSeeder::class,
            DocumentacaoSeeder::class,
            EnderecoSeeder::class,
            FuncionalSeeder::class,
            DependenteSeeder::class
        ]);
    }
}
