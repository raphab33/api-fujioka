<?php

use App\Models\Servidor\Documentacao;
use Illuminate\Database\Seeder;

class DocumentacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Documentacao::class, 10)->create();
    }
}
