<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {

    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');

    Route::group(['middlware' => 'auth'], function () {
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::get('me', 'AuthController@me');
    });
});

Route::get('endereco','EnderecoController@index');
Route::get('endereco/{id}','EnderecoController@show');
Route::post('endereco','EnderecoController@store');
Route::put('endereco/{id}','EnderecoController@update');
Route::delete('endereco/{id}','EnderecoController@delete');

Route::get('nota','NotaController@index');
Route::get('nota/{id}','NotaController@show');
Route::post('nota','NotaController@store');
Route::put('nota/{id}','NotaController@update');
Route::delete('nota/{id}','NotaController@delete');

Route::get('curso','CursoController@index');
Route::get('curso/{id}','CursoController@show');
Route::post('curso','CursoController@store');
Route::put('curso/{id}','CursoController@update');
Route::delete('curso/{id}','CursoController@delete');

Route::get('turma','TurmaController@index');
Route::get('turma/{id}','TurmaController@show');
Route::post('turma','TurmaController@store');
Route::put('turma/{id}','TurmaController@update');
Route::delete('turma/{id}','TurmaController@delete');

Route::get('aluno','AlunoController@index');
Route::get('aluno/{id}','AlunoController@show');
Route::post('aluno','AlunoController@store');
Route::put('aluno/{id}','AlunoController@update');
Route::delete('aluno/{id}','AlunoController@delete');

Route::get('users', 'AuthController@index');
Route::get('users/{user_id}', 'AuthController@show');
Route::put('users/{user_id}', 'AuthController@update');
Route::patch('users/{user_id}', 'AuthController@update');
